<?php
require ('core/FPDF/fpdf.php');


class Invoice_model extends FPDF

{




    public function generatePDF($fullname, $address, $zipcode, $city, $oid, $basket, $date, $total_price){
        ob_clean();
        define('EURO',chr(128));
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Image('images/logo/nav-logo.png', 85,0);
        $pdf->SetY(0);
        $pdf->SetFontSize(11);
        $pdf->Cell(0, 20, "Flevosap B.V.", 0, 1, 'R');
        $pdf->SetY(10);
        $pdf->SetFontSize(11);
        $pdf->Cell(0, 20, "Professor Zuurlaan 22,", 0, 1, 'R');
        $pdf->SetY(20);
        $pdf->SetFontSize(11);
        $pdf->Cell(0, 20, "8256 PE Biddinghuizen", 0, 1, 'R');
        $pdf->SetY(30);
        $pdf->SetFontSize(11);
        $pdf->Cell(0, 20, "Tel: 0321 331 742", 0, 1, 'R');
        $pdf->SetY(40);
        $pdf->SetFontSize(11);
        $pdf->Cell(0, 20, "info@flevosap.nl", 0, 1, 'R');
        $pdf->SetY(50);
        $pdf->SetFontSize(15);
        $pdf->Cell(0, 20, "T.a.v. " . $fullname, 0, 1, '');
        $pdf->SetY(60);
        $pdf->Cell(0, 20, $address . ", " . $zipcode . " " . $city, 0, 1, '');
        $pdf->SetY(70);
        $pdf->Cell(0, 20, "Datum: " . $date, 0, 1, '');
        $pdf->SetY(85);
        $pdf->Cell(0, 20, "Bestellingsnummer: " . $oid, 0, 1, '');
        $pdf->SetY(100);
        for ($i = 0; $i < count($basket); $i++){
            $pdf->Cell(100, 20, $basket[$i][1], 1, 0, '');
            $pdf->Cell(0, 20, $_SESSION['basket'][$basket[$i][0]][0], 1, 1, 'C');
        }
        $pdf->SetY($pdf->GetY() + 15);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(0, 20, "Totale prijs: " . EURO . $total_price, 0, 1, '');
        $pdf->Image('images/invoice/Group 3.png', 0,$pdf->GetY() + 5, $pdf->GetPageWidth(), 40);
        return $pdf->Output('S', $oid . ".pdf");
    }

}
