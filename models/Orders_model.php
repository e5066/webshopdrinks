<?php

require_once 'core/db_connection.php';

class Orders_model extends db_connection
{
    public function getOrders($uid)
    {
        $orders = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT orders.order_date, orders.oid, orders.price, products.name, order_items.quantity FROM order_items INNER JOIN orders on orders.id = order_items.oid INNER JOIN products on products.id = order_items.pid WHERE orders.uid = $uid;");
        while ($row = $stmt->fetch()) {
            array_push($orders, array($row['order_date'], $row['oid'], $row['price'], $row['name'], $row['quantity']));
        }
        return $orders;
    }
}