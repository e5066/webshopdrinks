<?php

require_once 'core/db_connection.php';

class Shop_model extends db_connection
{
    //When vistors/users go to the shop page this method will run to fill the shop with products.
    public function getShopContent()
    {
        $products = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query("SELECT * FROM products ORDER BY LENGTH(products.name) ASC;");
        while ($row = $stmt->fetch()) {
            array_push($products, array($row['id'], $row['name'], $row['price'], $row['capacity'], $row['description'], $row['n_value'], $row['pic'], $row['stock']));
        }
        return $products;
    }

    //The reason this is private is because we use this method only inside of it's own class in another function,
    //the attribute $sort is set in the controller from the sort values.
    //It wil return the query that suites the $sort value.
    private function buildQuery($sort){
        switch ($sort){
            case "prijs":
                return "SELECT * FROM `products` ORDER BY products.price DESC;";
            case "prijslow":
                return "SELECT * FROM `products` ORDER BY products.price ASC;";
            case "top10":
                return "SELECT products.id, products.name, products.price, products.capacity, products.description, products.n_value, products.pic, products.stock FROM products INNER JOIN order_items ON order_items.pid = products.id ORDER BY order_items.quantity DESC";
            case "az":
                return "SELECT * FROM `products` ORDER BY products.name ASC;";
            case "za":
                return "SELECT * FROM `products` ORDER BY products.name DESC;";
            case "family":
                return "SELECT * FROM `products` WHERE fv = 1 AND capacity = '1L';";
            case "onroad":
                return "SELECT * FROM `products` WHERE fv = 1 AND capacity != '1L';";
            case "vegg":
                return "SELECT * FROM `products` WHERE fv = 2;";
            default:
                return "SELECT * FROM products ORDER BY LENGTH(products.name) ASC;";
        }
    }

    //When user wants to sort this method will run and gets the query from buildQuery, to give the correct products from the database.
    public function filterShop($sort)
    {
        $products = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->query($this->buildQuery($sort));
        while ($row = $stmt->fetch()) {
            array_push($products, array($row['id'], $row['name'], $row['price'], $row['capacity'], $row['description'], $row['n_value'], $row['pic'], $row['stock']));
        }
        return $products;
    }

}


