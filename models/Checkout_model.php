<?php

require_once 'core/db_connection.php';

class Checkout_model extends db_connection
{
    public $test;
    protected $orderPlaced;

    public function __construct()
    {
        $this->test = "";
        $this->orderPlaced = false;
    }


    private function insertOrder($oid,$uid, $address, $zipCode, $city, $province, $price)
    {
        $sql = "INSERT INTO orders (oid, uid, address, zipcode, city, province, price, discount) VALUES (:oid, :uid, :address, :zipCode, :city, :province, :price, 2);";

        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":oid", $oid, PDO::PARAM_STR);
            $stmt->bindParam(":uid", $uid, PDO::PARAM_STR);
            $stmt->bindParam(":address", $address, PDO::PARAM_STR);
            $stmt->bindParam(":zipCode", $zipCode, PDO::PARAM_STR);
            $stmt->bindParam(":city", $city, PDO::PARAM_STR);
            $stmt->bindParam(":province", $province, PDO::PARAM_STR);
            $stmt->bindParam(":price", $price, PDO::PARAM_STR);
            //$stmt->bindParam(":discount", $discount, PDO::PARAM_STR);

            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }

    private function selectOrderId($oid)
    {
        $sql = "SELECT orders.id from orders where orders.oid = :oid";

        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":oid", $oid, PDO::PARAM_STR);

            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                while ($row = $stmt->fetch()) {
                    return $row['id'];
                }

            } else {
                return -1;
            }
        }

    }

    public function placeOrder($oid, $uid, $address, $zipcode, $city, $province, $price, $basket)
    {
        (empty($uid) ? $uid = 5 : '');
        if ($this->insertOrder($oid, $uid, $address, $zipcode, $city, $province, $price)){
            $currentId = $this->selectOrderId($oid);
            if ($currentId != -1){
                foreach ($basket as $key => $value){
                    $sql = "INSERT INTO order_items (oid, pid, quantity) VALUES (:id, :pid, :quantity);";
                    if($stmt=$this->connect()->prepare($sql)){
                        $id = intval($currentId);
                        $keyC = intval($key);
                        $quantity = intval($basket[$key][0]);
                        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
                        $stmt->bindParam(":pid", $keyC, PDO::PARAM_INT);
                        $stmt->bindParam(":quantity", $quantity, PDO::PARAM_INT);

                        if($stmt->execute()) {
                            $this->orderPlaced = true;
                        }
                        else {
                            $this->orderPlaced = false;
                        }
                    }
                }
                return $this->orderPlaced;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    //function for forms autofill
    public function selectInfoLoggedUser($userId)
    {
        $sql = "SELECT firstname, lastname, email, tel, address, zipcode from users where id = :userId;";
        $userCreds = array();
        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":userId", $userId, PDO::PARAM_INT);
            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                while ($row = $stmt->fetch()) {
                    array_push($userCreds, $row['firstname'], $row['lastname'], $row['email'], $row['tel'], $row['address'], $row['zipcode']);
                }
            } else {
                array_push($userCreds, "none");
            }
        }
        return $userCreds;
    }

    public function insertLoggedUid($userId)
    {
        $sql = "INSERT INTO orders (oid, uid, address, zipcode, city, province, price, discount) VALUES (1, :uid,1, 1, 1, 1, 1, 2);";

        if ($stmt = $this->connect()->prepare($sql)) {
            // Bind variables to the prepared statement as parameters

            $stmt->bindParam(":uid", $userId, PDO::PARAM_STR);




            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }




}