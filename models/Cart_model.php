<?php

require 'core/db_connection.php';

class Cart_model extends db_connection
{
    /**
     * This method will collect all column values of the product where id = id "The id is an attribute that is set in the cart controller".
     */
    public function getProduct($id) {
        $product = array();
        $dbconn = new db_connection();
        $stmt = $dbconn->connect()->prepare("SELECT * FROM `products` WHERE id = :id");
        $stmt->execute([':id' => $id]);
        while ($row = $stmt->fetch()) {
            array_push($product, $row['id'], $row['name'], $row['price'], $row['capacity'], $row['description'], $row['n_value'], $row['pic'], $row['stock']);
        }
        return $product;
    }
}