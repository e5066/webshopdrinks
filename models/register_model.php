<?php

include_once './core/db_connection.php';

class register_model extends db_connection
{
    private array $user;

    public function __construct()
    {
        $this->user = array();
    }

    public function validateUser($mail): string
    {
        try {
            $stmt = $this->connect()->prepare("SELECT password FROM users WHERE email = :mail");
            $stmt->execute([':mail' => $mail]);
            $pw = "";
            while ($row = $stmt->fetch()) {
                $pw = $row['password'];
            }
            return $pw;
        } catch (PDOException $ex) {
            return $ex;
        }
    }

    public function login($mail, $pw)
    {
        try {
            $stmt = $this->connect()->prepare("SELECT id, firstname FROM users WHERE email = :mail AND password = :pw");
            $stmt->execute([':mail' => $mail, ':pw' => $pw]);
            while ($row = $stmt->fetch()) {
                array_push($this->user, $row['id'], $row['firstname']);
            }
            return $this->user;
        } catch (PDOException $ex) {
            return $ex;
        }
    }

    public function validateIfUserExists($mail)
    {
        $sql = "SELECT id FROM users WHERE email = :mail";
        if ($stmt = $this->connect()->prepare($sql)) {
            $stmt->bindParam(":mail", $mail, PDO::PARAM_STR);
            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    return 1;
                } else {
                    return 0;
                }
            }
            else {
                return 2;
            }
        }
    }

    public function registerAccount($firstname, $lastname, $email, $tel, $username, $password, $street, $hsnumber, $zipcode){
        $address = $street .', '. $hsnumber;

        $sql = "INSERT INTO users (username, firstname, lastname, email, password, tel, address, zipcode)
                VALUES (:username, :firstname, :lastname, :email, :password, :tel, :address, :zipcode)";

        if($stmt = $this->connect()->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
            $stmt->bindParam(":lastname", $lastname, PDO::PARAM_STR);
            $stmt->bindParam(":email", $email, PDO::PARAM_STR);
            $stmt->bindParam(":password", $password, PDO::PARAM_STR);
            $stmt->bindParam(":tel", $tel, PDO::PARAM_STR);
            $stmt->bindParam(":address", $address, PDO::PARAM_STR);
            $stmt->bindParam(":zipcode", $zipcode, PDO::PARAM_STR);




            if($stmt->execute()) {
                return 5;
            }
            else{
                return 10;
            }
        }
    }

}