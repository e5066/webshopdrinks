<?php

include_once './core/db_connection.php';

class forgot_password_model extends db_connection
{
    public function getUserName($mail): string
    {
        try {
            $stmt = $this->connect()->prepare("SELECT CONCAT(firstname, ' ', lastname) as fullname FROM users WHERE email = :mail");
            $stmt->execute([':mail' => $mail]);
            $name = "";
            while ($row = $stmt->fetch()) {
                $name = $row['fullname'];
            }
            echo $name;
            return $name;
        } catch (PDOException $ex) {
            return $ex;
        }
    }

    public function resetPwOfUser($email, $password){
        try {
            $sql = "UPDATE users SET password = :password WHERE email = :email";
            if($stmt = $this->connect()->prepare($sql)){
                // Bind variables to the prepared statement as parameters
                $stmt->bindParam(":email", $email, PDO::PARAM_STR);
                $stmt->bindParam(":password", $password, PDO::PARAM_STR);
                // Attempt to execute the prepared statement
                if($stmt->execute()) {
                    return true;
                }
                else{
                    return false;
                }
            }
        } catch (PDOException $ex) {
            return false;
        }
    }
}