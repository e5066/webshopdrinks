<head>
    <link href="/flevosap/styling/home.css" rel="stylesheet">
</head>
<body style="font-family: 'Montserrat', sans-serif !important;">
    <div class="container-fluid">
        <div class="row p-auto">
            <div class="col-12 p-0 text-center">
                <img class="img-fluid billboard" src="/flevosap/images/achtergrond/homeBillboard.png">
            </div>
        </div>
        <div class="row pt-5 pb-5 align-items-center">
            <div class="d-flex col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
                <div class="col-12">
                    <h1 class="ps-lg-5 pe-lg-5 text-wrap">Liefhebbers van Flevosap: welkom!</h1>
                    <p class=" ps-lg-5 pe-lg-5 text-wrap">
                        De vele variaties zorgen voor de ene fruitige verrassing na de andere. De pure smaak van één soort, of een spannende combinatie van twee of meer. Altijd 100 procent natuurlijk, dus zonder smaak- en conserveringsmiddelen. Waar je ook bent, met Flevosap heb je altijd de smaak te pakken!
                    </p>
                    <div class="pt-lg-5 pb-lg-5"></div>
                    <h1 class="ps-lg-5 pe-lg-5 text-wrap">Altijd en overal genieten</h1>
                    <p class="ps-lg-5 pe-lg-5 text-wrap">
                        De grote flessen kom je tegen in je supermarkt. Dat is makkelijk meenemen, en kinderen zijn er dol op. Volwassenen trouwens ook. In de winkel vind je ook een handig klein flesje dat graag mee onderweg gaat. Zoals naar (sport)school of werk, een dagje uit, of in je rugzak op vakantie.
                        <br>
                        Gezellig samen wat eten en drinken in een café, lunchroom of restaurant? Ook daar kun je genieten van de smaak van vers fruit. Om zeker te weten dat je de echte krijgt, bestel je niet zo maar een sapje, maar vraag je om Flevosap!
                    </p>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 ps-5 pe-5 text-center">
                <img class="img-fluid welcome-img ps-5 pe-5" src="/flevosap/images/gallery/galley08.png">
            </div>
        </div>
        <div class="row container-second ps-md-3 pe-md-3 ps-lg-0 pe-lg-0">
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 col-xxl-4 ps-md-5 pe-md-5 ps-lg-0 pe-lg-0">
                <h2 class="text-wrap">De fles mag er ook zijn!</h2>
                <p class="text-wrap">
                    De echte Flevosap herken je al van verre aan de bijzondere fles. Doordat er geen statiegeld op de fles zit kun je er iets leuks mee doen. Als vaas bijvoorbeeld of kaarsenstandaard. Of maak je eigen rozenwater. Daarna wil de Flevosapfles graag in de glasbak om hergebruikt te worden.
                </p>
            </div>
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 col-xxl-4 ps-md-5 pe-md-5 ps-lg-0 pe-lg-0">
                <h2 class="text-wrap">Geen conserveringsmiddelen</h2>
                <p class="text-wrap">
                    Doordat we zoveel mogelijk goede stoffen bewaren, is Flevosap troebel. Daarom zeggen we dus met trots: het sap waar je de smaak ziet zitten. Bovendien bevat Flevosap geen conserveringsmiddelen. Door te pasteuriseren wordt het sap, zonder gebruik te maken van toevoegingen, houdbaar gemaakt.
                </p>
            </div>
            <div class="col-12 col-md-12 col-lg-12 col-xl-4 col-xxl-4 ps-md-5 pe-md-5 ps-lg-0 pe-lg-0">
                <h2 class="text-wrap">Volgens Zwitserse traditie</h2>
                <p class="text-wrap">
                    Het fruit voor Flevosap groeit niet alleen in de frisse buitenlucht, het fruit voor Flevosap wordt ook midden tussen de fruitbomen geperst. Écht ambachtelijk! Een speciale machine vermaalt de hele vrucht met schil en al tot pulp. Daarna wordt het sap volgens Zwitserse traditie met een bandenpers tot op de laatste druppel uitgeperst. Vervolgens gaat het sap door een zeef, maar niet teveel: alleen de grove delen worden eruit gehaald. De vezels horen namelijk bij het sap en dragen bij aan een lagere cholesterol, betere spijsvertering en er zo lang mogelijk jong uitzien (en dat wil jij toch ook?).
                </p>
            </div>
        </div>
        <div class="row ps-lg-5 pe-lg-5 align-items-center" id="contact-homepage">
            <div class="col-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 pt-5 pb-5">
                <h1>Vragen? stuur ons gerust een bericht!</h1>
                <img class="img-fluid become-member-img pt-4" src="/flevosap/images/glazen/Transparant/Winteravond-aangepast.png">
            </div>
            <div class="col-12 col-md-6 col-lg-6 col-xl-6 col-xxl-6 pt-5 pb-5">
                <form class="form-controls" method="post">
                    <div class="form-floating mb-3">
                        <input name="firstname" type="text" class="form-control" id="floatingInput" placeholder="Voornaam">
                        <label for="floatingInput">Voornaam</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input name="lastname" type="text" class="form-control" id="floatingPassword" placeholder="Achternaam">
                        <label for="floatingPassword">Achternaam</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input name="mail" type="email" class="form-control" id="floatingPassword" placeholder="Uw mail">
                        <label for="floatingPassword">Uw mail</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input name="subject" type="text" class="form-control" id="floatingPassword" placeholder="Onderwerp">
                        <label for="floatingPassword">Onderwerp</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea name="question" class="form-control" placeholder="Laat hier Uw vraag achter" id="floatingTextarea2" style="height: 100px"></textarea>
                        <label for="floatingTextarea2">Laat hier Uw vraag achter</label>
                    </div>
                    <div class="col-auto float-end">
                        <button type="submit" class="btn btn-primary mb-3 send-btn p-3">Verstuur</button>
                    </div>
                </form>
                <?php if($success != "") : ?>
                    <h2 class="text-center mt-3"><?= $success ?></h2>
                <?php else : ?>
                    <h2 class="text-center mt-3"><?= $failed ?></h2>
                <?php endif; ?>
            </div>
        </div>
    </div>
</body>
