<head>
    <link href="/flevosap/styling/checkout.css" rel="stylesheet">
</head>
<body>
<div class="container content-container mb-5">

    <div class="row mt-4 ms-5 me-5">
        <div class="row mt-4 ms-0">
        <div class="col-12">
            <a href="javascript:history.back()" id="backBtn"><i class="bi bi-arrow-return-left" style="font-size: 24px" title="Terug"></i></a>
        </div>
        </div>
        <div class="test col-sm-12 col-md-12 col-lg-2 mt-5 ">
            <div class="col justify-content-center">
                <img class="img-fluid img-apple" src="/flevosap/images/logo/nav-logo.png" alt="my help">
            </div>
        </div>

        <div class="col-sm-12 col-md-12 mt-5 col-lg-10 mt-5 ">
            <img class="img-fluid img-bar " src="/flevosap/images/achtergrond/bar_checkout.png" alt="my bar">
        </div>

        <div class="col-sm-12 col-md-6 col-lg-4 mt-5 mb-4 ">

            <p class="h4 part1 mt-2">Verzendadres:</p>

            <form method="post">

                <div class="form-floating mb-3">
                    <input id="voornaam" type="text" class="form-control space1 mb-3"
                           name="firstname" placeholder="name@example.com" value="<?= (isset($_SESSION['firstname']) ? $_SESSION['firstname'] : $firstName )?>">
                    <label for="floatingInput">Voornaam</label>
                </div>

                <div class="form-floating mb-3">
                    <input id="achternaam" type="text"  class="form-control space1 mb-3 "
                           name="lastname" placeholder="Mario" style="padding-right: 50px;" onchange="this.form.submit();" value="<?= (isset($_SESSION['lastname']) ? $_SESSION['lastname'] : $lastName)?>">
                    <label for="floatingInput">Achternaam:</label>
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text">postcode</span>
                    <input type="text" class="form-control space2" placeholder="" name="zipcode" aria-label="Postcode" value="<?= (isset($_SESSION['zipcode']) ? $_SESSION['zipcode'] : $zipCode) ?>">
                    <span class="input-group-text">huisnr</span>
                    <input type="text" class="form-control space2" placeholder="" name="hnr" aria-label="Postcode" onchange="this.form.submit();" value="<?= (isset($_SESSION['hnr']) ? $_SESSION['hnr'] : $hnr) ?>">
                </div>

                <div class="form-floating mb-3">
                    <input id="straat" type="text" class="form-control space1 mb-3"
                           name="str" placeholder="Schutterstraat 34" value="<?= (isset($_SESSION['street']) ? $_SESSION['street'] : $street) ?>" disabled readonly>
                    <label for="straat" >Straat:</label>
                </div>

                <div class="form-floating mb-3">
                    <input id="plaats" type="text" class="form-control space1 mb-3"
                        name="city" placeholder="Rotterdam" value="<?= (isset($_SESSION['city']) ? $_SESSION['city'] : $city) ?>" disabled readonly>
                   <label for="plaats" >Plaats:</label>
                </div>

                <div class="form-floating mb-3">
                    <input id="land" type="text" class="form-control space1 mb-3"
                           name="country" value="<?= (isset($_SESSION['province']) ? $_SESSION['province'] : $province) ?>" disabled readonly>
                    <label for="land" >Provincie</label>
                </div>


            <p class="h4 part1 mt-5">Contactgegevens:</p>

            <div class="form-floating mb-3">
                <input id="email" type="text" class="form-control space1 mb-3"
                       placeholder="" name="email" onchange="this.form.submit();" value="<?= (isset($_SESSION['email']) ? $_SESSION['email'] : $email) ?>">
                <label for="email" >Email:</label>
            </div>

            <div class="form-floating mb-3">
                <input id="telefoonNr" type="text" class="form-control space1 mb-3"
                   placeholder="" name="phoneNr" onchange="this.form.submit();"  value="<?= (isset($_SESSION['phoneNr']) ? $_SESSION['phoneNr'] : $phoneNr) ?>">
                <label for="telefoonNr" >Telefoonnr:</label>
            </div>

            </form>
        </div>


        <div class="col-sm-12 col-md-6 col-lg-4 mt-5">


            <p class="h4 part1 mt-2">Factuurgegevens:</p>

            <div class="form-check">
                <input id="sameAddress"
                       type="checkbox"
                       class="form-check-input">
                <label  class="form-check-label" for="sameAddress">zoals verzendadres</label>
            </div>

            <p class="h4 part1 mt-4">Extra opties</p>

            <label for="lastName" class="form-label mt-4">Voeg een bericht toe aan de verzending</label>
            <input id="lastName" type="text"
                   class="form-control space1"
                   placeholder="">
            <div class="invalid-feedback">
                Valid first name is required
            </div>

            <label for="lastName" class="form-label mt-3">Voeg een kortingscode toe</label>
            <input id="lastName" type="text"
                   class="form-control space1"
                   placeholder="">
            <div class="invalid-feedback">
                Valid first name is required
            </div>



        </div>


        <div class="col-sm-12 col-md-6 col-lg-4 mt-5">
            <p class="h4 part1 mt-2">Uw bestelling:</p>
            <div class="col-12" style="overflow-y: scroll; tab-index: 0; height: 15rem;">
                <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-offset="0" class="scrollspy-example" tabindex="0">
                    <ol class="list-group list-group-numbered">
                    <?php $total_price = 0; $counter = 0; for($i = 0; $i < count($cart_items); $i++) : ?>
                        <li class="list-group-item d-flex justify-content-between align-items-start">
                            <div class="ms-2 me-auto">
                                <div class="fw-bold"><?= $cart_items[$i][1] ?>
                                </div>

                            </div>
                            <span class="badge bg-primary rounded-pill quantity-pill"><?= $_SESSION['basket'][$cart_items[$i][0]][0] ?></span>
                        </li>
                        <?php endfor;?>
                    </ol>
                </div>
            </div>
            <hr class="my-4">
            <p class="h6 mt-2">Verzending(via POSTNL)</p>
            <hr class="my-4">
            <p class="h4 mt-2">TOTAAL
                €<?= number_format((float)$_SESSION['total-price'], '2', ',', ',') ?>

            <h4 class="mb-3 part1 mt-5 text-centers">BETALINGSINFORMATIE</h4>



            <div class="form-check">
                <div class="dropdown">
                    <select class="btn btn-secondary form-select" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        iDEAL

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">


                        <option selected>iDEAL</option>
                        <option value="1" href="#">ING</option>
                        <option value="2" href="#">RABOBANK</option>
                        <option value="3" href="#">ABN AMRO</option>
                        <option value="4" href="#">SNS</option>
                        <option value="5" href="#">ASN</option>
                        <option value="6" href="#">Knab</option>
                        <option value="7" href="#">Bunq</option>

                    </ul>
                    </select>
                </div>
            </div>
            <form class="contact-for" method="post">
                <div class="form-group">
                    <input class="btn-checkout btn-dark mt-5 ms-5 checkoutBtn" id="btn-checkout" name="goToCheckout" type="submit" value="Betalen">
                </div>
            </form>

        </div>
    </div>

</div>

</body>
