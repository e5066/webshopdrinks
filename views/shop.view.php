<head>
    <link href="/flevosap/styling/shop.css" rel="stylesheet">
</head>
<body>
    <div class="container-fluid shop-content">
        <div class="">
            <div class="row first-row mb-4">
                <div class="col-8 ms-md-5 ms-lg-5 mt-5">
                    <h1 class="text-start">Altijd overal genieten</h1>
                </div>
            </div>
            <div class="row d-inline-flex">
                <div class="col-1 col-sm-1 col-md-1 col-lg-1 align-items-start p-auto">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle sortBtn" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Sorteren
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=az">A-Z</a></li>
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=za">Z-A</a></li>
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=top10">Meest verkocht</a></li>
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=prijs">Prijs hoog - laag</a></li>
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=prijslow">Prijs laag - hoog</a></li>
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=family">Voor de familie</a></li>
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=onroad">Voor onderweg</a></li>
                            <li><a class="dropdown-item" href="/flevosap/shop?sort=vegg">Voor de groenteliefhebbers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center justify-content-sm-center justify-content-md-between justify-content-lg-between empty-space pb-5">
            <?php if (!empty($products)) : for ($i = 0; $i < count($products); $i++) : ?>
            <div class="col-10 col-lg-3 col-md-5 col-sm-10 mt-5 ms-2 me-2 d-flex align-items-start flex-fill">
                <div class="card product-cards">
                    <div class="card-header product-header">
                        <h2 id="<?= $products[$i][0] ?>"><?= $products[$i][1] ?></h2>
                    </div>
                    <div class="card-body product-items">
                        <h5 class="card-subtitle mb-2 text-muted float-end">€<?= $products[$i][2] ?> <?= $products[$i][3] ?></h5>
                        <button class="m-0 float-start p-0" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal<?=$i?>" style="border: none transparent; background-color: transparent; width: 100%">
                        <?php echo '<img src="data:image;base64,'.base64_encode($products[$i][6]).'" class="img-fluid float-start" alt="Image" title="Bekijk '.$products[$i][1].'" style="width: 250px; height: 100%;">'?>
                        </button>
                        <div class="card-text row d-flex">
                            <div class="col-5 align-self-center">
                                <form method="post" class="mb-0">
                                    <input id="<?= $products[$i][0] ?>" type="number" name="quantity" placeholder="1" value="1" class="form-control <?= $products[$i][0] ?>" min="1" onchange="setNewValue(this.value, this.id)">
                            </div>
                            <div class="col-7">
                                    <a href="/flevosap/shop/#<?= $products[$i][0] ?>"><button id="<?= $products[$i][0] ?>" class="btn add-to-cart" name="add-to-basket" value="<?= $products[$i][0] ?>" type="submit"><i class="bi bi-basket p-1" id="plusBtn" aria-hidden="true"></i></button></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal<?=$i?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header modal-background">
                                <h1 class="modal-title" id="exampleModalLabel"><?= $products[$i][1] ?></h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-5 col-lg-4 m-0 p-0 align-self-center">
                                        <?php echo '<img src="data:image;base64,'.base64_encode($products[$i][6]).'" class="img-fluid float-start" alt="Image" style="width: 100%; height: 80%;">'?>
                                    </div>
                                    <div class="col-sm-12 col-md-7 col-lg-8 m-0 p-0">
                                        <h3>Beschrijving:</h3>
                                        <p><?= $products[$i][4] ?></p>
                                        <br>
                                        <h3>Voedingswaarde:</h3>
                                        <p><?= $products[$i][5] ?></p>
                                        <br>
                                        <h6><i>Op voorraad: <?= $products[$i][7] ?></i></h6>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="input-group mb-3 mt-3 ms-lg-5 me-lg-5 ps-lg-5 pe-lg-5 row">
                                    <form method="post" class="input-group">
                                        <input id="<?= $products[$i][0] ?>" name="quantity" type="number" min="1" value="1" class="form-control col-2 <?= $products[$i][0] ?>" placeholder="1" aria-describedby="button-addon2" onchange="setNewValue(this.value, this.id)">
                                        <button class="btn btn-outline-secondary col-3 addBtn" type="button" id="<?= $products[$i][0] ?>-add" onclick="addQuantity(this.value, this.id)" value="1">+</button>
                                        <button class="btn btn-outline-secondary col-3 minBtn" type="button" id="<?= $products[$i][0] ?>-min" onclick="minQuantity(this.value, this.id)" value="1">-</button>
                                        <a href="/flevosap/shop/#<?= $products[$i][0] ?>" class="btn add-to-cart col-4"><button id="<?= $products[$i][0] ?>" name="add-to-basket" value="<?= $products[$i][0] ?>" class="add-to-cart-modal ps-2 pe-3 ps-sm-3 pe-sm-3 ps-md-3 pe-md-3 ps-lg-3 pe-lg-3 ps-xl-3 pe-xl-3 ps-xxl-3 pe-xxl-3"><i class="bi bi-basket" id="plusBtn2" aria-hidden="true"></i></button></a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <script>
                function setNewValue(val, id){
                    currentInput = document.getElementById(id).id;
                    results = document.getElementsByClassName(currentInput);
                    Array.prototype.forEach.call(results, function (result){
                        result.value = parseInt(val);
                    });
                }
                function addQuantity(val, id){
                    currentBtn = document.getElementById(id).id;
                    id = currentBtn.split("-");
                    results = document.getElementsByClassName(id[0]);
                    Array.prototype.forEach.call(results, function (result){
                        result.value = result.value? parseInt(result.value) + parseInt(val) : parseInt(val);
                    });
                }
                function minQuantity(val, id){
                    currentBtn = document.getElementById(id).id;
                    id = currentBtn.split("-");
                    results = document.getElementsByClassName(id[0]);
                    Array.prototype.forEach.call(results, function (result){
                        if((parseInt(result.value) - parseInt(val)) > 0) {
                            result.value = result.value ? parseInt(result.value) - parseInt(val) : parseInt(val);
                        }
                    });
                }
            </script>
            <?php endfor; else : ?>

            <?php endif; ?>
        </div>
    </div>
</body>
