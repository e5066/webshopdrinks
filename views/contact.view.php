<?php
?>

<head>
    <link href="/flevosap/styling/contact.css" rel="stylesheet">
</head>

<body>
<div class="container mt-5 navbar-space">
    <div class="row d-flex justify-content-center">
        <form class="contact-for col-12 col-sm-12 col-md-6 col-lg-6 justify-content-center text-center" method="post">
            <div class="form-row">
                <div class="form-group">
                    <h2 class="text-center mb-5">Neem contact met ons op.</h2>
                    <label for="inputFirstName"></label>
                    <input type="text" name="firstName" class="form-control" id="inputFirstName" placeholder="Voornaam">
                </div>
                <div class="form-group text-center mt-3">
                    <label for="inputLastName"></label>
                    <input type="text" name="lastName" class="form-control" id="inputLastName" placeholder="Achternaam">
                </div>
            </div>
            <div class="form-group mt-3">
                <label for="inputAddress"></label>
                <input type="text" name="mail" class="form-control" id="inputEmail" placeholder="Uw email?">
            </div>
            <div class="form-group mt-3">
                <label for="inputAddress"></label>
                <input type="text" name="subject" class="form-control" id="inputSubject" placeholder="Onderwerp">
            </div>
            <div class="form-group text-center mt-3">
                <label for="exampleFormControlTextarea1"></label>
                <textarea class="form-control" name="message" id="exampleFormControlTextarea1" placeholder="Uw vragen." rows="3"></textarea>
            </div>
            <div class="row d-flex mt-4">
                <div class="col justify-content-center text-center">
                    <input type="submit" name="submit" class="btn-send m-0 pt-2 pb-2" id="btn-message" value="Verstuur">
                </div>
            </div>
        </form>
        <?php if($success != "") : ?>
            <h2 class="text-center mt-3"><?= $success ?></h2>
        <?php else : ?>
            <h2 class="text-center mt-3"><?= $failed ?></h2>
        <?php endif; ?>
    </div>
</div>
</body>

