<!DOCTYPE html>
<head>
    <link href="/flevosap/styling/login.forgot_password.css" rel="stylesheet">
</head>
<body>

<div style="flex: 1 0 auto">
    <img src="/flevosap/images/headerimg.png" class="img-fluid">
    <div class="container mt-2">
        <div class="title">Inloggen!</div>
        <div class="content">
            <form action="#" method="post">
                <div class="user-details">
                    <div class="input-box">
                        <span class="details">E-mailadres</span>
                        <input type="text" placeholder="Emailadres invoeren" name="email" required>
                    </div>
                    <div class="input-box">
                        <span class="details">Wachtwoord</span>
                        <input type="password" placeholder="Wachtwoord invoeren" name="password" required>
                    </div>
                </div>
                <div class="button">
                    <input type="submit" value="Inloggen" name="loginBtn">
                </div>
            </form>
            <div style="position: center">
                <a href="#" data-bs-toggle="modal" data-bs-target="#wachtwoord_vergeten">Wachtwoord vergeten?</a>
                <a href="/flevosap/register">Nog geen account?</a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="wachtwoord_vergeten" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Wachtwoord vergeten</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form name="forgotForm" method="post" onsubmit="return validateForm()" required>
                    <div class="mb-3">
                        <label class="col-form-label">Vul hier uw emailadres in:</label>
                        <input type="text" class="form-control" id="forgotmail" name="forgotmail" required>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-auto d-flex">
                            <h4 style=" margin-top: 10px; text-align: center;"><?= $mail_msg ?></h4>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="resetBtn">Verzenden</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>