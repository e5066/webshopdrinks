<head>
    <link href="/flevosap//styling/orders.css" rel="stylesheet">
</head>
<body>
    <div class="container mt-5 mb-5">
        <div class="row d-flex">
            <div class="col-12">
                <h1>Bestellingen:</h1>
            </div>
        </div>
        <div class="row d-flex mt-5 mb-5">
            <div class="col-12">
                <?php if ($orders != null) : ?>
                    <div class="accordion" id="accordionExample">
                        <?php for ($i = 0; $i < count($orders); $i++) : ?>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne<?=$i?>" aria-expanded="true" aria-controls="collapseOne">
                                        <?= "Order: " . $orders[$i][$i][1] ?><br>
                                        <?= "Aankoopdatum: " . $orders[$i][$i][0] ?>
                                    </button>
                                </h2>
                                <div id="collapseOne<?=$i?>" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <table class="table table-danger table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Product</th>
                                                    <th scope="col">Hoeveelheid</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php for ($y = 0; $y < count($orders[$i]); $y++) :?>
                                                <tr>
                                                    <th scope="row"><?=$y + 1?></th>
                                                    <td><?= $orders[$i][$y][3] ?></td>
                                                    <td><?= $orders[$i][$y][4] ?></td>
                                                </tr>
                                            <?php endfor; ?>
                                            </tbody>
                                        </table>
                                        <h3 class="text-end"><?="Betaald: €" . number_format((float)$orders[$i][0][2], '2', ',', ',') ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                <?php else: ?>
                    <h1>Nog geen bestellingen</h1>
                <?php endif; ?>
            </div>
        </div>
    </div>
</body>