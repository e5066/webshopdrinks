<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
(isset($_POST['loginBtn']) ? header("Location: $actual_link/flevosap/login") : "");
(isset($_POST['registerBtn']) ? header("Location: $actual_link/flevosap/register") : "")
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flevosap - <?= $page_title ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="/flevosap/styling/navigation.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/flevosap/">
            <img id="navbar-logo" src="/flevosap/images/logo/nav-logo.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse ms-lg-4" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item pe-lg-4">
                    <a class="nav-link" href="/flevosap/">Home</a>
                </li>
                <li class="nav-item pe-lg-4">
                    <a class="nav-link" href="/flevosap/shop">Shop</a>
                </li>
                <li class="nav-item pe-lg-4">
                    <a class="nav-link" href="/flevosap/over-ons">Over ons</a>
                </li>
                <li class="nav-item pe-lg-4">
                    <a class="nav-link" href="/flevosap/nieuws">Nieuws</a>
                </li>
                <li class="nav-item pe-lg-4">
                    <a class="nav-link" href="/flevosap/contact">Contact</a>
                </li>
            </ul>


            <form class="d-flex mb-0 justify-content-center row" method="post">
                <button id="basketBtn" class="col-12 col-md-12 col-lg-2 me-2" name="go-to-cart">
                    <a class="cart position-relative d-inline-flex" aria-label="Winkelwagen" title="Winkelwagen">
                        <i class="bi bi-basket align-middle basket-icon"></i>
                        <span id="basketCounter" class="cart-basket d-flex align-items-center justify-content-center">
                            <?= $_SESSION['basket-count']; ?>
                        </span>
                    </a>
                </button>
                <?php if (isset($_SESSION['userId'])) : ?>
                    <div class="col-12 col-md-12 col-lg-6 ms-lg-4">
                        <button class="btn btn-outline-success nav-btn ps-4 pe-4" id="loginBtn" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"><?= $_SESSION['userName'] ?></button>
                        <ul class="dropdown-menu dropdown-menu-lg-end" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#">Account</a></li>
                            <li><a class="dropdown-item" href="/flevosap/bestellingen">Bestellingen</a></li>
                            <li onclick="logout()"><a class="dropdown-item" href="#">Uitloggen</a></li>
                        </ul>
                    </div>
                <?php else: ?>
                <button class="btn btn-outline-success m-2 nav-btn col-6 col-md-6 col-lg-3" id="loginBtn" type="submit" name="loginBtn">Login</button>
                <button class="btn btn-outline-success m-2 nav-btn col-6 col-md-6 col-lg-5" id="registerBtn" type="submit" name="registerBtn">Registreren</button>
                <?php endif; ?>
            </form>
        </div>
    </div>
</nav>
<script>
    function logout(){
        var queryParams = new URLSearchParams(window.location.search);
        queryParams.set("logout", "true");
        history.replaceState(null, null, "?"+queryParams.toString());
        location.reload();
    }
</script>
<!-- Bootstrap scripts -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>