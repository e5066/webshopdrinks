<?php
?>

<head>
    <link href="/flevosap/styling/news.css" rel="stylesheet">
</head>

<body>

<div class="container-fluid style=" margin-top: 20px; padding-top: 10px; padding-bottom: 10px">
    <div class="row">
        <img class="img-fluid img-bar" src="/flevosap/images/news/news_slider.jpg" alt="my bar" >
    </div>

    <div class="row background1">
        <div class="row col-sm-12 col-md-12 col-lg-4 text-center mt-5 ms-1">
            <img class="img-fluid text-center img-news p-0" src="/flevosap/images/news/teler1.jpg" alt="my bar">
        </div>
            <div class="row col-sm-12 col-md-12 col-lg-8">
                <div class="class p-5 mt-4"><h3>‘Ik ervaar het hele jaar geluksmomentjes’</h3></div>
                    <div class="span p-5 text-break"><p>Sander van de Rijdt teelt met veel vakmanschap en passie appels en peren.
                            ‘De kale bomen die in bloei komen en uiteindelijk vol sappig fruit hangen, dat blijft ieder jaar weer een mooi proces’, legt hij uit.
                            ‘Als ik dan door de boomgaard loop en zie hoeveel fruit er hangt, word ik daar heel blij van.’
                            De appels en peren die uiteindelijk te groot zijn, of juist te klein, een kleine beschadiging of een hagelpitje hebben, zijn perfect voor Flevosap.
                            Bij Sander staat altijd wel een fles in de koelkast. ‘Dat Flevosap mijn buurman is, maakt me wel een beetje trots.
                            Het is trouwens ook een goed gekozen naam.’ Over de smaak is hij kort en krachtig: ‘Zoals je het verwacht: puur natuur. Heerlijk!’</p>
                    </div>
            </div>
    </div>

    <div class="row background2">
        <div class="row col-sm-12 col-md-12 col-lg-8 ">
            <div class="class p-5 mt-4"><h3>Appelbomen zo ver als het oog reikt</h3></div>
            <div class="span p-5"><p>Opa Jaap en oma Corrie kwamen vanuit het Zuid-Hollandse Abbenbroek naar Flevoland, waar ze in Dronten een fruitteeltbedrijf begonnen.
                    Anno nu lijkt het fruitteeltbedrijf van de familie Rozendaal recht uit een kinderboek.
                    Een grote hond met één gebogen oor genaamd Max drentelt gezellig over het erf.
                    Twee inktzwarte Dahomey dwergstieren staan in een weilandje genoeglijk te herkauwen.
                    En natuurlijk zie je appelbomen zo ver als het oog reikt. ‘We hebben in totaal 26 hectare boomgaard, met daarin voornamelijk Elstar-appels’, legt Stefan trots uit.
                    ‘Die zijn goed voor 20 hectare. Daarnaast hebben we ook nog ongeveer 4,5 hectare Conference-peren en 1,5 hectare Junami-appels.
                    In totaal oogsten we elk jaar ongeveer een miljoen kilo fruit.’</p>
            </div>
        </div>

        <div class="row col-sm-12 col-md-12 col-lg-4 mt-5 ms-1">
            <img class="img-fluid img-news p-0" src="/flevosap/images/news/teler2.jpg" alt="my bar" >
        </div>
    </div>

    <div class="row background3">
        <div class="row col-sm-12 col-md-12 col-lg-4 mt-5 ms-1">
            <img class="img-fluid img-news p-0" src="/flevosap/images/news/green_organics.jpg" alt="my bar" >
        </div>
    <div class="row col-sm-12 col-md-12 col-lg-8">
        <div class="class p-5 mt-4"><h3>Dagvers dankzij winterslaap</h3></div>
        <div class="span p-5"><p>In tegenstelling tot veel andere fruittelers, sorteert de familie Rozendaal de appels zelf.
                Ook worden de appels op het bedrijf zelf opgeslagen.
                In september en oktober zijn er ongeveer 50 mensen in de boomgaard aan het plukken.
                Wanneer de appels van de boomgaard in Dronten geplukt zijn, gaan ze dezelfde dag nog in de koelcel.
                De cel wordt vervolgens hermetisch afgesloten, waarna het zuurstofgehalte wordt teruggebracht naar minder dan 0,5 procent.
                ‘Dat zorgt ervoor, dat de appels in een soort winterslaap gaan.
                Ze rijpen niet verder, en blijven even vers als de dag waarop we ze plukten.’
                Voor Stefan is er geen beter gevoel dan het moment na de pluk. ‘De appels zijn dan veilig binnen. Dat is waar we het hele jaar naartoe hebben gewerkt.’</p>
        </div>
    </div>
    </div>






</div>



</body>
