<head><link href="/flevosap//styling/footer.css" rel="stylesheet"></head>

<footer class=" footer mt-auto" style="background-color: #FDEDE8; margin-top: 20px; padding-top: 10px; padding-bottom: 10px">
    <div class="row m-auto">
        <div class="col-12 col-sm-6 align-self-center">
            <img src="/flevosap/images/logo/nav-logo.png" width="159px" height="auto">
        </div>
        <div class="col-12 col-sm-3 align-self-center">
            <h3>Contact</h3>
            <h6>Flevosap B.V.</h6>
            <h6>Professor Zuurlaan 22,</h6>
            <h6>8256 PE Biddinghuizen</h6>
            <h6><a href="tel: 0321 331 742">Tel: 0321 331 742</a></h6>
            <h6><a href="mailto: info@flevosap.nl">info@flevosap.nl</a></h6>
        </div>
        <div class="col-12 col-sm-3">
            <h3>Social media</h3>
            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </div>
    </div>
</footer>
