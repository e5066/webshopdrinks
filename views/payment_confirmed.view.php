<?php
?>

<head>
    <link href="/flevosap/styling/payment_confirmed.css" rel="stylesheet">
</head>
<body>

    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center mb-5"><?="Bedankt voor uw bestelling " . $_SESSION['firstname'] . "."?></h1>
                <h5 class="text-center"><?="De factuur is verstuurd naar uw mail: " . $_SESSION['email']?></h5>
            </div>
            <div class="thank_you d-flex col-12 justify-content-center">
                <a href="/flevosap/shop"><input class="btn-home btn-dark mt-5 ps-5 pe-5 backHomeBtn text-center"  name="goToShop"  type="submit" value="Terug naar de Shop"></a>
            </div>
            <div class="thank_you d-flex col-12 justify-content-center mb-5 pb-5">
                <a href="/flevosap/"><input class="btn-home btn-dark mt-5 ps-5 pe-5 backShopBtn text-center"  name="goToHome"  type="submit" value="Terug naar de Home"></a>
            </div>
        </div>
    </div>








</body>
