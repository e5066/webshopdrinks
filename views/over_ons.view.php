<html lang="en">
<head>
    <link href="/flevosap/styling/over_ons.css" rel="stylesheet">
</head>
<body>

<img src="/flevosap/images/overige/overons.png" style="max-width: 100%">


<div class="container my-1 mt-1 over-ons-content">
    <div class="row justify-content-center">
     <p class="headtitle">Over ons</p>
        <p><strong>Over smaak valt niet te twisten…</p>

        <hr>

        <p class="text-1">…zegt het spreekwoord. Nou, daar zijn ze het bij Flevosap dus mooi
        niet altijd mee eens.<br> Want als het op proeven aankomt, heeft iedereen een
        eigen idee over de smaak.<br> Maar na dagen proeven, proeven en nog eens proeven
        komen ze er altijd uit.<br> Dan hebben ze precies de echte Flevosapsmaak te pakken.</p>

        <p><strong>Perfecte match</p>

        <hr>

        <p class="text-2">Wat dat is? Puur vakmanschap van jarenlange ervaring in het telen van fruit op de<br>
        tikkeltje zilte Flevolandse bodem, boordevol zeemineralen. <br>Dat geeft natuurlijk het beste
        en sappigste fruit van de gulste bomen.<br> Bij Flevosap weten ze precies
        welke fruitsoorten wel en welke niet matchen.</p>
    </div>
</div>
</body>
</html>
