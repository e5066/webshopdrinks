<!DOCTYPE html>
<head>
    <link href="/flevosap/styling/register.css" rel="stylesheet">
</head>
<body>
<div style="flex: 1 0 auto">
    <img src="/flevosap/images/rrr.png" class="img-fluid">
<div class="container mt-3">
    <div class="title">Wordt lid!</div>
    <div class="content">
        <form action="#" method="post">
            <div class="user-details">
                <div class="input-box">
                    <span class="details">Gebruikersnaam</span>
                    <input type="text" placeholder="Invoeren gebruikersnaam" name="username" required>
                </div>
                <div class="input-box">
                    <span class="details">Wachtwoord</span>
                    <input type="password" placeholder="Invoeren wachtwoord" name="password" required>
                </div>
                <div class="input-box">
                    <span class="details">Voornaam</span>
                    <input type="text" placeholder="Invoeren voornaam" name="firstname" required>
                </div>
                <div class="input-box">
                    <span class="details">Achternaam</span>
                    <input type="text" placeholder="Invoeren achternaam"  name ="lastname" required>
                </div>
                <div class="input-box">
                    <span class="details">E-mail</span>
                    <input type="text" placeholder="Invoeren emailadres" name="email" required>
                </div>
                <div class="input-box">
                    <span class="details">Telefoonnummer</span>
                    <input type="text" placeholder="Invoeren telefoonnummer" name="tel" required>
                </div>
                <div class="input-box">
                    <span class="details">Straatnaam</span>
                    <input type="text" placeholder="Invoeren straat" name="street" required>
                </div>
                <div class="input-box">
                    <span class="details">Postcode</span>
                    <input type="text" placeholder="Invoeren postcode" name="zipcode" required>
                </div>
                <div class="input-box">
                    <span class="details">Huisnummer</span>
                    <input type="text" placeholder="Invoeren huisnummer" name="hsnumber" required>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-md-auto d-flex">
                    <h4 style=" margin-top: 10px; text-align: center;"><?= $error_msg ?></h4>
                </div>
            </div>

            <div class="button">
                <input type="submit" value="Lid worden!" name="register">
            </div>
        </form>

    </div>
</div>

</body>
</html>
<!--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>-->
</body>
