<head>
    <link href="/flevosap/styling/cart.css" rel="stylesheet">
</head>
<body class="background-cart">
    <div class="container cart-items-card mt-5 mb-5 ms-auto me-auto">
        <div class="row mt-4 ms-0">
            <div class="col-12">
                <a href="/flevosap/shop" id="backBtn"><i class="bi bi-arrow-return-left" style="font-size: 24px" title="Terug"></i></a>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-2 col-md-12 col-sm-12 mt-4 d-flex justify-content-center">
                <div class="col-12 d-flex justify-content-center">
                    <img src="/flevosap/images/logo/nav-logo.png" alt="Logo">
                </div>
            </div>
            <div class="col-lg-10 col-md-12 col-sm-12 d-flex justify-content-center mt-4">
                <img class="process-header" src="/flevosap/images/overige/processWinkelwagen.png" alt="process" width="100%" height="auto"/>
            </div>
        </div>
        <?php if(count($_SESSION['basket'])) :?>
            <div class="row">
                <div class="col-12" style="overflow-y: scroll; tab-index: 0; height: 25rem;">
                    <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-offset="0" class="scrollspy-example" tabindex="0">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product</th>
                                <th scope="col">Hoeveelheid</th>
                                <th scope="col">Prijs</th>
                                <th scope="col">Verwijder</th>
                            </tr>
                            </thead>
                            <?php $total_price = 0; $counter = 0; for($i = 0; $i < count($cart_items); $i++) : ?>
                            <?php $counter += $_SESSION['basket'][$cart_items[$i][0]][0] ?>
                            <tbody>
                            <tr>
                                <th scope="row"><?php echo '<img src="data:image;base64,'.base64_encode($cart_items[$i][6]).'" class="img-fluid float-start" alt="Image" title="Bekijk '. $cart_items[$i][1] .'" style="width: 95px; height: auto;">'?></th>
                                <td  class="align-middle"><?= $cart_items[$i][1] ?></td>
                                <form method="post">
                                <td  class="align-middle"><input name="quantity" id="<?=$cart_items[$i][0]?>" type="number" value="<?= $_SESSION['basket'][$cart_items[$i][0]][0] ?>" onchange="setNewValue(this.value, this.id)" onfocusout="location.reload();"></td>
                                <td  class="align-middle">€<?= number_format((float)$cart_items[$i][2] * $_SESSION['basket'][$cart_items[$i][0]][0], '2', ',', ','); $total_price += ($cart_items[$i][2] * $_SESSION['basket'][$cart_items[$i][0]][0])?></td>
                                <td  class="align-middle"><button style="background-color: transparent; border: none transparent;" id="<?= $cart_items[$i][0] ?>" onclick="removeFromCart(this.id)" title="Verwijder <?= $cart_items[$i][1] ?>"><i class="bi bi-trash removeFromCartBtn" style="font-size: 36px"></i></button></td>
                                </form>
                            </tr>
                            </tbody>
                            <script>
                                function setNewValue(val, id){
                                    currentInput = document.getElementById(id).id;
                                    results = document.getElementsByClassName(currentInput);
                                    Array.prototype.forEach.call(results, function (result){
                                        result.value = parseInt(val);
                                    });
                                    var queryParams = new URLSearchParams(window.location.search);
                                    queryParams.set("id", id);
                                    queryParams.set("quantity", val);
                                    history.replaceState(null, null, "?"+queryParams.toString());
                                }
                                function removeFromCart(id){
                                    var queryParams = new URLSearchParams(window.location.search);
                                    queryParams.set("remove", id);
                                    history.replaceState(null, null, "?"+queryParams.toString());
                                    location.reload();
                                    <?php $_SESSION['basket-count'] = 0;?>
                                }
                            </script>
                    <?php endfor; $_SESSION['basket-count'] = $counter?>
                            <tbody>
                                <tr>
                                    <form method="post">
                                        <td class="align-middle"><a href="/flevosap/cart"><input class="btn deleteBtn" name="deleteAll" type="submit" value="Leeg winkelmand"></a></td>
                                    </form>
                                    <td></td>
                                    <td></td>
                                    <td class="align-middle">Totaal: €<?=number_format((float)$total_price, '2', ',', ',')?></td>
                                    <td class="align-middle"><a href="/flevosap/checkout"><button class="btn checkoutBtn">Checkout</button></a></td>
                                </tr>
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php $_SESSION['total-price'] = $total_price; else : ?>
            <div class="row justify-content-center d-flex">
                <div class="col-12">
                    <div class="p-5"></div>
                </div>
                <div class="col-12">
                    <h1 class="text-center">Geen producten in het winkelmandje<br><a href="/flevosap/shop">Shop</a></h1>
                    <?php $_SESSION['basket-count'] = 0; ?>
                </div>
                <div class="col-12">
                    <div class="p-5"></div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</body>
