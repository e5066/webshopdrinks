<?php
if ( !isset($_SESSION) ) session_start();
if (empty($_SESSION['basket-count'])) {
    $_SESSION['basket-count'] = 0;
    $_SESSION['basket'] = [];
}
if (isset($_POST['go-to-cart'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/flevosap/cart");
}
if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}
$page_title = "Home";

require 'core/Php_mail_sender.php';

global $success, $failed;
$success = "";
$failed = "";

if (!empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['mail']) && !empty($_POST['question']) && !empty($_POST['subject'])){
    $firstName=$_POST['firstname'];
    $lastName=$_POST['lastname'];
    $mail=$_POST['mail'];
    $subject=$_POST['subject'];
    $message=$_POST['question'];

    $message = "
<h1 style='visibility: hidden'>Vraag: $subject</h1>
<h4>$message</h4>
<br>
<I>With kind regards,
<br>
$firstName $lastName<br>
$mail <I>
<br>
<img src='https://www.pngarts.com/files/12/Call-Centre-Agent-PNG-Free-Download.png'><p style='visibility: hidden'></p>
";
    ?>    <?php
    Php_mail_sender::sendQuestionMail($_POST['firstname'],$_POST['lastname'], $_POST['mail'],$_POST['subject'], $message);
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $success = Php_mail_sender::$success;
    $failed = Php_mail_sender::$failed;
    ?>    <?php ?>    <meta http-equiv="Refresh" content="8; url=/flevosap/#contact-homepage">
    <?php ?>    <?php
    $failed="";
}
elseif(isset($_POST['submit']) == "Verzenden" ){
    $failed = "Verplichte velden zijn leeg!";
}

require 'views/navigation.php';
require 'views/home.view.php';
require 'views/footer.php';

