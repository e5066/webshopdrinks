
<?php
session_start();
// Turn off all error reporting
error_reporting(0);
$page_title = "Checkout";
global $cart_items;
$cart_items = [];
require_once 'models/Cart_model.php';
$cart_model = new Cart_model();
require_once 'models/Invoice_model.php';
$pdf_creator = new Invoice_model();
require_once 'core/Php_mail_sender.php';
$php_mailer = new Php_mail_sender();

if (isset($_SESSION['basket'])){
    foreach ($_SESSION['basket'] as $key => $value){
        array_push($cart_items,  $cart_model->getProduct($key));
    }
}

if (isset($_POST['go-to-cart'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/flevosap/cart");
}

require_once 'models/Checkout_model.php';

$checkout_model = new Checkout_model();

global $json, $firstName, $lastName, $zipCode, $email, $place, $hnr, $city, $province, $phoneNr, $street ;

//checks if user is logged in
if (!empty($_SESSION['userId'])){
    //var_dump($_SESSION['userId']);
    $userCreds = $checkout_model->selectInfoLoggedUser($_SESSION['userId']);
    $_SESSION['firstname'] = $userCreds[0];
    $_SESSION['lastname'] = $userCreds[1];
    $_SESSION['email'] = $userCreds[2];
    $_SESSION['phoneNr'] = $userCreds[3];
    $_SESSION['zipcode'] = $userCreds[5];
    $houseNr=$userCreds[4];
    $addressArray = explode(',', $houseNr);
    $_SESSION['hnr'] = $addressArray[1];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);

    $data = [
        "postcode" => $_SESSION['zipcode'],
        "number" => $_SESSION['hnr'],
    ];

    curl_setopt($ch, CURLOPT_URL, "https://postcode.tech/api/v1/postcode/full?" . http_build_query($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: Bearer 5deb5e64-786b-441c-a728-211ee7274d07",
    ));

    $response = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($response, true);

    $street = $json['street'];
    $city = $json['city'];
    $province = $json['province'];
    $_SESSION['street'] = $street;
    $_SESSION['province'] = $province;
    $_SESSION['city'] = $city;

}

if (!empty($_POST['firstname']) && !empty($_POST['lastname'])){
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
}

if (!empty($_POST['hnr']) && !empty(['zipcode'])) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);

    $data = [
        "postcode" => $_POST['zipcode'],
        "number" => $_POST['hnr'],
    ];

    curl_setopt($ch, CURLOPT_URL, "https://postcode.tech/api/v1/postcode/full?" . http_build_query($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization: Bearer 5deb5e64-786b-441c-a728-211ee7274d07",
    ));

    $response = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($response, true);

    $_SESSION['zipcode']=$_POST['zipcode'];
    $street = $json['street'];
    $city = $json['city'];
    $province = $json['province'];
    $_SESSION['hnr']=$_POST['hnr'];
    $_SESSION['street'] = $street;
    $_SESSION['province'] = $province;
    $_SESSION['city'] = $city;

    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
}

if (!empty($_POST['email'])){
    $_SESSION['email'] = $_POST['email'];
}

if (!empty($_POST['phoneNr'])){
    $_SESSION['phoneNr'] = $_POST['phoneNr'];
}


if(isset($_POST['goToCheckout'])) {

    $oid = date('d-m-y H:i:s');
    $seperators = ['" "', '-', ':'];

    $ytoid = str_replace($seperators, $seperators[0], $oid);

    $oidNew = explode($seperators[0], $ytoid);
    $ytOidNew = "";
    for ($i = 0; $i < count($oidNew); $i++) {
        $ytOidNew .= $oidNew[$i];
    }

    $newOidKey = explode(' ', $ytOidNew);
    $currentOid = $newOidKey[0] . $newOidKey[1] . $_SESSION['zipcode'] . $_SESSION['hnr'];
    $totalPrice = str_replace(',', '.', $_SESSION['total-price']);
    if ($checkout_model->placeOrder($currentOid, $_SESSION['userId'] ,$_SESSION['street'] . ", " . $_SESSION['hnr'], $_SESSION['zipcode'], $_SESSION['city'], $_SESSION['province'], $totalPrice, $_SESSION['basket'])) {
        $_SESSION['order'] = $_SESSION['firstname'] . " bedankt voor uw aankoop, U vindt de factuur terug in uw mail.";
        $php_mailer->sendInvoice($_SESSION['firstname'], $_SESSION['lastname'], "Bestelling: " . $currentOid . " - Flevosap", "Bedankt voor uw bestelling", $_SESSION['email'], $pdf_creator->generatePDF($_SESSION['firstname'] . " " . $_SESSION['lastname'], $_SESSION['street'] . ", " . $_SESSION['hnr'],  $_SESSION['zipcode'], $_SESSION['city'], $currentOid, $cart_items, $oid, $_SESSION['total-price']), $currentOid);
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
        header("Location: $actual_link/flevosap/payment_confirmed");

    } else {
        $_SESSION['order'] = "Excuses er is iets fout gegaan. Neem contact op met de helpdesk.";
   }
}

if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}

require 'views/navigation.php';
require 'views/checkout.view.php';
require 'views/footer.php';
