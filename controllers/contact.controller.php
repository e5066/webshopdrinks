<?php
if ( !isset($_SESSION) ) session_start();
$page_title = "Contact";
//Create the necessary sessions that we later need, if not exists.
if (empty($_SESSION['basket-count'])){
    $_SESSION['basket-count'] = 0;
    $_SESSION['basket'] = [];
}

//Count basket quantity for each item, and set session to show counter in navigation with new values.
if (count($_SESSION['basket'])){
    $total_price = 0;
    $counter = 0;
    foreach ($_SESSION['basket'] as $key => $value){
        for ($i = 0; $i < count($_SESSION['basket'][$key]); $i++){
            $counter += $_SESSION['basket'][$key][0];
        }
    }
    $_SESSION['basket-count'] = $counter;
}
//If basket in navigation is pressed bring us to the cart page.
if (isset($_POST['go-to-cart'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/flevosap/cart");
}
if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}

global $user_name;
require 'core/Php_mail_sender.php';

global $success, $failed;

$success = "";
$failed = "";

if (!empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['mail']) && !empty($_POST['message']) && !empty($_POST['subject'])){

    $firstName=$_POST['firstName'];
    $lastName=$_POST['lastName'];
    $mail=$_POST['mail'];
    $subject=$_POST['subject'];
    $message=$_POST['message'];


    $message = "
<h1 style='visibility: hidden'>
Vraag: $subject</h1>
<h4>$message</h4>
<br>
<I>With kind regards,
<br>
$firstName $lastName
<br>
$mail <I>
<br>
<img src='https://www.pngarts.com/files/12/Call-Centre-Agent-PNG-Free-Download.png'>
<p style='visibility: hidden'></p>
";

    ?>
    <?php
    Php_mail_sender::sendQuestionMail($_POST['firstName'],$_POST['lastName'], $_POST['mail'],$_POST['subject'], $message);
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $success = Php_mail_sender::$success;
    $failed = Php_mail_sender::$failed;
    ?>
    <?php ?>
    <meta http-equiv="Refresh" content="5; url=/flevosap/contact">
    <?php ?>
    <?php
    $failed="";
}
elseif(isset($_POST['submit']) == "Verzenden" ){
    $failed = "Not all required fields are filled in!";
}



require 'views/navigation.php';
require 'views/contact.view.php';
require 'views/footer.php';