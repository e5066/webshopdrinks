<?php
if ( !isset($_SESSION) ) session_start();
$user_name = "Sign in";
$page_title = "Login";
require 'models/register_model.php';
require 'core/Php_mail_sender.php';
require 'models/forgot_password_model.php';


global $mail_err, $login_err, $user, $status, $disabled, $mail_msg;

$lr_model = new register_model();
$reset_class = new forgot_password_model();
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

if(isset($_COOKIE['auth_timer']) && $_COOKIE['auth_timer'] == true){
    $status = "disabled";
    $disabled = "disabled";
}
else{
    $status = "";
    $disabled = "";
}
(!isset($_SESSION['loginCount']) ? $_SESSION['loginCount'] = 0 : "");
if (isset($_POST['loginBtn']) && !empty($_POST['email'])){
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $mail_err = "Invalid mail";
    }
    else
    {
        if ($_POST['password']){
            if (!empty($lr_model->validateUser($_POST['email']))){
                $hashed_pw = $lr_model->validateUser($_POST['email']);
                if (password_verify($_POST['password'], $hashed_pw)){
                    if(!empty($lr_model->login($_POST['email'], $hashed_pw))) {
                        $user = $lr_model->login($_POST['email'], $hashed_pw);
                        $_SESSION['userId'] = $user[0];
                        $_SESSION['userName'] = $user[1];
                        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
                        header("Location: $actual_link/flevosap/shop");
                    }
                    else {
                        $login_err = "Account kon niet gevonden worden. Neem contact op.";
                    }
                }
                else{
                    if ($_SESSION['loginCount'] < 3){
                        switch ($_SESSION['loginCount']){
                            case 0:
                                $login_err = "Wachtwoord onjuist, 3 pogingen over";
                                break;
                            case 1:
                                $login_err = "Wachtwoord onjuist, 2 pogingen over";
                                break;
                            case 2:
                                $login_err = "Wachtwoord onjuist, 1 poging over";
                                break;
                        }
                        $_SESSION['loginCount'] += 1;
                    }
                    else{
                        $login_err = "Pogingen overschreden, probeer het later nog eens";
                        setcookie('auth_timer', true, time()+60*120);
                        header("Refresh:3");
                    }
                }
            }
            else {
                $login_err = "Account kon niet gevonden worden met huidige email";
            }
        }
        else{
            $login_err = "Wachtwoord niet ingesteld";
        }
    }
}
else{
    (isset($_POST['loginBtn']) ? $login_err = "Invul velden zijn leeg" : null);
}

//Vanaf hier is het wachtwoord vergeten gedeelte
if (isset($_POST['resetBtn']) && !empty($_POST['forgotmail'])){
    $characters = '0123456789@!$abcdefghijklmnopqrstuvwxyz@!$ABCDEFGHIJKLMNOPQRSTUVWXYZ@!$';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 14; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    $fullname = $reset_class->getUserName($_POST['forgotmail']);
    $message = "<h1>Beste $fullname,</h1>
                <h3>Nieuw wachtwoord: <I>$randomString</I></h3>" . "<br>" .
        "<a href='$actual_link/flevosap/login/forgot_password'>Volg de link om in te loggen met nieuw wachtwoord</a>" . "<br>" .
        "<br><I>Met vriendelijke groeten,</I> <br>" .
        "<br><I>WFFLIX - Team B2</I>";
    if (empty($fullname)){
        $color = "red";
        $mail_msg = "Account kon niet gevonden worden";
    }
    else{
        Php_mail_sender::forgotPassword($_POST['forgotmail'], $fullname, $message);
        if (!empty(Php_mail_sender::$failed)){
            $color = "red";
            $mail_msg = Php_mail_sender::$failed;
        }
        elseif(!empty(Php_mail_sender::$success)){
            if ($reset_class->resetPwOfUser($_POST['forgotmail'], password_hash($randomString, PASSWORD_DEFAULT))){
                $color = "green";
                $mail_msg = Php_mail_sender::$success;
                header("Refresh:3");
            }
            else{
                $color = "red";
                $mail_msg = "Wachtwoord kon niet gereset worden,<br> neem contact met onss op!";
            }
        }
        else{
            $color = "red";
            $mail_msg = "Er is iets fout gegaan!";
        }
    }
}
else{
    if(isset($_POST['resetBtn'])){
        $color = "red";
        $mail_msg = "Email is niet ingevuld!";
    }
}
//Create the necessary sessions that we later need, if not exists.
if (empty($_SESSION['basket-count'])){
    $_SESSION['basket-count'] = 0;
    $_SESSION['basket'] = [];
}

//Count basket quantity for each item, and set session to show counter in navigation with new values.
if (count($_SESSION['basket'])){
    $total_price = 0;
    $counter = 0;
    foreach ($_SESSION['basket'] as $key => $value){
        for ($i = 0; $i < count($_SESSION['basket'][$key]); $i++){
            $counter += $_SESSION['basket'][$key][0];
        }
    }
    $_SESSION['basket-count'] = $counter;
}
//If basket in navigation is pressed bring us to the cart page.
if (isset($_POST['go-to-cart'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/flevosap/cart");
}
if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}
require 'views/navigation.php';
require 'views/login.view.php';
require 'views/footer.php';
