<?php
//Checks if session is set, if not it will set the session.
if ( !isset($_SESSION) ) session_start();
error_reporting(0);
global $cart_items;
$cart_items = [];

//Instantiate Cart_model class.
require_once 'models/Cart_model.php';
$cart_model = new Cart_model();
if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}
//Navigation title needs to be set.
$page_title = "Cart";
require 'views/navigation.php';

//If session basket exists fill the global array $cart_items with the products from the database,
//the reason we do this is to make sure we don't send request to the database too often.
if (isset($_SESSION['basket'])){
    foreach ($_SESSION['basket'] as $key => $value){
        array_push($cart_items,  $cart_model->getProduct($key));

    }
}

//Set a new quantity using the cart quantity option, the quantity and ID is caught by this if statement from the url.
if (!empty($_GET['id'])){
    if ($_SESSION['basket'][$_GET['id']][0] != $_GET['quantity']){
        $_SESSION['basket'][$_GET['id']][0] = $_GET['quantity'];
        header("Refresh:0");
    }
}

//Id is a getter this id is set in javascript to remove the item using the recycle bin.
if (isset($_GET['remove'])){
    unset($_SESSION['basket'][$_GET['remove']]);
    echo '<meta http-equiv="Refresh" content="0; url=/flevosap/cart">';
}

if (isset($_POST['deleteAll'])){
    $_SESSION['basket'] = [];
    echo '<meta http-equiv="Refresh" content="0; url=/flevosap/cart">';
}

require 'views/cart.view.php';
require 'views/footer.php';

