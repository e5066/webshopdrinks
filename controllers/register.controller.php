<?php
if ( !isset($_SESSION) ) session_start();
global $error_msg;
$page_title = "Register";
require 'models/register_model.php';
//Create the necessary sessions that we later need, if not exists.
if (empty($_SESSION['basket-count'])){
    $_SESSION['basket-count'] = 0;
    $_SESSION['basket'] = [];
}



//Count basket quantity for each item, and set session to show counter in navigation with new values.
if (count($_SESSION['basket'])){
    $total_price = 0;
    $counter = 0;
    foreach ($_SESSION['basket'] as $key => $value){
        for ($i = 0; $i < count($_SESSION['basket'][$key]); $i++){
            $counter += $_SESSION['basket'][$key][0];
        }
    }
    $_SESSION['basket-count'] = $counter;
}
//If basket in navigation is pressed bring us to the cart page.
if (isset($_POST['go-to-cart'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/flevosap/cart");
}

if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}

require 'views/navigation.php';

$regex_pattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,32}$/";


$register_model = new register_model();

if (isset($_POST['register'])) {

    if (empty($_POST['firstname']) || empty($_POST['lastname']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['tel']) || empty($_POST['username']) || empty($_POST['street']) || empty($_POST['zipcode'] || empty($_POST['hsnumber']))){
        $color = "red";
        $error_msg = "Velden niet ingevuld";
    }
    else {
        $user_exists = $register_model->validateIfUserExists($_POST['email']);
        echo $user_exists;
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $color = "red";
            $error_msg = "Email niet geldig.";
        }
        else if ($user_exists != 0) {
            if ($user_exists == 1) $error_msg = "Account bestaat al.";
            if ($user_exists == 2) $error_msg = "Er is iets fout gegaan!";
        }
        else if (!preg_match($regex_pattern, $_POST['password'])){
            $color = "red";
            $error_msg = "Minimaal 8 tot 32 tekens, waaronder een hoofdletter,<br>een kleine letter, een nummer en een speciale teken.";
        }
        else{

            if ($register_model->registerAccount($_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['tel'],
                    $_POST['username'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['street'],$_POST['hsnumber'], $_POST['zipcode']) == true){
                $color = "green";

                $error_msg = "Welkom! je bent geregistreerd";
                $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
                header("Location: $actual_link/flevosap/shop");
            }
        }
    }
}

require 'views/register.view.php';
require 'views/footer.php';