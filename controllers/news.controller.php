<?php
if ( !isset($_SESSION) ) session_start();
$page_title = "Nieuws";
//Create the necessary sessions that we later need, if not exists.
if (empty($_SESSION['basket-count'])){
    $_SESSION['basket-count'] = 0;
    $_SESSION['basket'] = [];
}

//Count basket quantity for each item, and set session to show counter in navigation with new values.
if (count($_SESSION['basket'])){
    $total_price = 0;
    $counter = 0;
    foreach ($_SESSION['basket'] as $key => $value){
        for ($i = 0; $i < count($_SESSION['basket'][$key]); $i++){
            $counter += $_SESSION['basket'][$key][0];
        }
    }
    $_SESSION['basket-count'] = $counter;
}
//If basket in navigation is pressed bring us to the cart page.
if (isset($_POST['go-to-cart'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/flevosap/cart");
}
if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}
require 'views/navigation.php';
require 'views/news.view.php';
require 'views/footer.php';