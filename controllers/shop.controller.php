<?php
//Check if session exists if not make a session.
if ( !isset($_SESSION) ) session_start();

//Require necessary model class to create an instance.
require_once 'models/Shop_model.php';
$shop_model = new Shop_model();

global $products, $add_item;

//Navigation title needs to be set.
$page_title = "Shop";

//Create the necessary sessions that we later need, if not exists.
if (empty($_SESSION['basket-count']) || !isset($_SESSION['basket-count']) || !isset($_SESSION['basket'])){
    $_SESSION['basket-count'] = 0;
    $_SESSION['basket'] = [];
}

//Count basket quantity for each item, and set session to show counter in navigation with new values.
if (count($_SESSION['basket'])){
    $total_price = 0;
    $counter = 0;
    foreach ($_SESSION['basket'] as $key => $value){
        for ($i = 0; $i < count($_SESSION['basket'][$key]); $i++){
            $counter += $_SESSION['basket'][$key][0];
        }
    }
    $_SESSION['basket-count'] = $counter;
}

if (isset($_GET['logout'])){
    if ($_GET['logout'] == "true"){
        unset($_SESSION['userId']);
        unset($_SESSION['userName']);
        session_commit();
    }
}

//Create a global products array to call it in the shop.view
if (empty($shop_model->getShopContent())){
    echo "Geen producten";
    $products = null;
}
else{
    $products = $shop_model->getShopContent();
}

//If we select sort this method will run a function based in the Shop_model.
if (isset($_GET['sort'])){
    $products = $shop_model->filterShop($_GET['sort']);
}

//Check if necessary arrays exists, also check if selected item is not already in the basket "if so replace the quantity of that item, else add new product".
if (count($products) != null){
    if (isset($_POST['add-to-basket'])){
        if (isset($_SESSION['basket-count'])){
            $key = array_key_exists($_POST['add-to-basket'], $_SESSION['basket']);
            if (!$key){
                $_SESSION['basket'][$_POST['add-to-basket']] = array($_POST['quantity']);
                $_SESSION['basket-count'] += intval($_POST['quantity']);
                if (isset($_GET['sort'])){
                    $page = "/flevosap/shop?sort=" . $_GET['sort'] . "#" . $_POST['add-to-basket'];
                }
                else{
                    $page = "/flevosap/shop?sort=" . $_GET['sort'] . "#" . $_POST['add-to-basket'];
                }
                $sec = "0";
                session_commit();
            }
            else{
                $_SESSION['basket'][$_POST['add-to-basket']][0] = $_POST['quantity'];
                $_SESSION['basket-count'] += intval($_POST['quantity']);
                if (isset($_GET['sort'])){
                    $page = "/flevosap/shop?sort=" . $_GET['sort'] . "#" . $_POST['add-to-basket'];
                }
                else{
                    $page = "/flevosap/shop?sort=" . $_GET['sort'] . "#" . $_POST['add-to-basket'];
                }
                $sec = "0";
                session_commit();
            }
            //The reload brings us back to the top of the page. We need to get back to the item we where so we set the location of the ID.
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
            header("Location: $actual_link$page");
        }
    }
}

//If basket in navigation is pressed bring us to the cart page.
if (isset($_POST['go-to-cart'])){
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    header("Location: $actual_link/flevosap/cart#", true, 301);
}
require 'views/navigation.php';
require 'views/shop.view.php';
require 'views/footer.php';
?>