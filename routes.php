<?php
$router->define([
    '' => 'controllers/home.controller.php',
    'register' => 'controllers/register.controller.php',
    'login' => 'controllers/login.controller.php',
    'over-ons' => 'controllers/over_ons.controller.php',
    'shop' => 'controllers/shop.controller.php',
    'cart' => 'controllers/cart.controller.php',
    'checkout' => 'controllers/checkout.controller.php',
    'factuur' => 'controllers/factuur.controller.php',
    'payment_confirmed'=>'controllers/payment_confirmed.controller.php',
    'nieuws' => 'controllers/news.controller.php',
    'bestellingen' => 'controllers/orders.controller.php',
    'contact' => 'controllers/contact.controller.php'
]);
