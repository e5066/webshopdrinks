<?php

class Request
{
    public static function uri()
    {
	$prefix="flevosap";
	$uri=$_SERVER['REQUEST_URI'];
	if(!empty($prefix) && $uri == ("/".$prefix))
	{
		header("Location: /".$prefix."/", true, 301);
		exit();
	}
	else if($uri == ("/".$prefix."/"))
	{
		$uri = "/";	
	}
	else if(substr($uri, 0, strlen("/".$prefix)) == ("/".$prefix))
	{
		$uri=substr($uri, strlen("/".$prefix)); 
	}
	
	return trim(parse_url($uri, PHP_URL_PATH),  '/');
    }
}
