<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';

class Php_mail_sender
{
    public static $success, $failed;

    public static function sendQuestionMail($firstname, $lastname, $fromMail, $subject,$message){
        //Create an instance; passing `true` enables exceptions$mail = new PHPMailer(true);
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                       //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->SMTPSecure = 'tls';
            $mail->Username   = 'wfflixb2@gmail.com';                   //SMTP username
            $mail->Password   = 'WijZijnTeam@B2!';                      //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`//Recipients
            $fullname = $firstname . " " . $lastname;
            $mail->setFrom($fromMail, $fullname );


            $mail->addAddress('wfflixb2@gmail.com', 'Team B2');     //Add a recipient$mail->addAddress('ellen@example.com');               //Name is optional$mail->addReplyTo('info@example.com', 'Information');



            //Attachments$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name//Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = 'vraag';
            $mail->Body    = $message;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
            Php_mail_sender::$success = "Bedankt voor Uw vraag, wij zullen binnen 24uur reageren.";
        } catch (Exception $e) {
            Php_mail_sender::$failed = "Excuses, er is iets verkeerd gegaan.";
        }
    }

    public function sendInvoice($firstname, $lastname, $subject ,$message, $toMail, $invoice, $oid){
        //Create an instance; passing `true` enables exceptions$mail = new PHPMailer(true);
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                       //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->SMTPSecure = 'tls';
            $mail->Username   = 'wfflixb2@gmail.com';                   //SMTP username
            $mail->Password   = 'WijZijnTeam@B2!';                      //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`//Recipients
            $fullname = $firstname . " " . $lastname;
            $mail->setFrom('wfflixb2@gmail.com', 'Team B2 - Flevosap');


            $mail->addAddress($toMail, $fullname);     //Add a recipient$mail->addAddress('ellen@example.com');               //Name is optional$mail->addReplyTo('info@example.com', 'Information');
            $mail->addStringAttachment($invoice, $oid . ".pdf");


            //Attachments$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name//Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            return "Bedankt voor je bestelling " . $firstname . " de factuur is verstuurd naar: " . $toMail;
        } catch (Exception $e) {
            return "Er is iets fout gegaan met de bestelling. Neem contact met ons op." . $e;
        }
    }

    public static function forgotPassword($mail_addr, $name, $message){
        //Create an instance; passing `true` enables exceptions$mail = new PHPMailer(true);
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                       //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->SMTPSecure = 'tls';
            $mail->Username   = 'wfflixb2@gmail.com';                   //SMTP username
            $mail->Password   = 'WijZijnTeam@B2!';                      //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`//Recipients
            $mail->setFrom("wfflixb2@gmail.com", "Team B2" );


            $mail->addAddress($mail_addr, $name);     //Add a recipient

            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = 'WFFLIX - Password reset.';
            $mail->Body    = $message;
            $mail->AltBody = 'Request new password.';

            $mail->send();
            Php_mail_sender::$success = "A message has been send to you're mail.";
        } catch (Exception $e) {
            Php_mail_sender::$failed = "Error in request, try again later." . "<br>" . $mail->ErrorInfo;
        }
    }

}